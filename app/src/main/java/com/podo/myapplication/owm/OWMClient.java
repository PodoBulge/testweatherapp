package com.podo.myapplication.owm;


import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Locale;


public class OWMClient {

    private String baseURL = "http://api.openweathermap.org/data/2.5/";

    OkHttpClient httpClient;

    public OWMClient() {}

    public OWMWeatherData currentWeatherAtCity(String city) throws IOException, JSONException
    {
        String subUrl = String.format(Locale.ROOT, "weather?q=%s&units=metric&lang=en", city);
        String fullUrl = baseURL + subUrl;
        String source = getResponseBody(fullUrl);
        JSONObject response = new JSONObject(source);
        int code = response.getInt("cod");
        if (code != 404) {
            return new OWMWeatherData(response);
        } else
            return null;
    }

    private String getResponseBody(String url) throws IOException
    {
        httpClient = new OkHttpClient();
        Request request = new Request.Builder()
                                     .url(url)
                                     .build();
        Response response = httpClient.newCall(request).execute();
        return response.body().string();
    }
}

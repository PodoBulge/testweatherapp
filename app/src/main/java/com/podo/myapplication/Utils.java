package com.podo.myapplication;

import android.graphics.Bitmap;
import android.graphics.Matrix;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;

public class Utils {
    public static CameraUpdate updateCamera(LatLng coords){
        CameraPosition camPos = new CameraPosition.Builder()
                                                  .target(coords)
                                                  .zoom(9.8f)
                                                  .build();
        CameraUpdate camUpdate = CameraUpdateFactory.newCameraPosition(camPos);

        return camUpdate;
    }

    public static Bitmap getResizedBitmap(Bitmap bm, int newHeight, int newWidth) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        Matrix matrix = new Matrix();
        matrix.postScale(scaleWidth, scaleHeight);
        Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height, matrix, false);

        return resizedBitmap;
    }
}

package com.podo.myapplication;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.podo.myapplication.owm.OWMClient;
import com.podo.myapplication.owm.OWMWeatherData;

import org.json.JSONException;

import java.io.IOException;
import java.net.URL;

public class WeatherActivity extends ActionBarActivity
                             implements TextWatcher, TextView.OnEditorActionListener {

    private static final String HUMIDITY_KEY = "humidity";
    private static final String L_HUMIDITY_KEY = "humidityLabel";
    private static final String PRESSURE = "pressure";
    private static final String L_PRESSURE = "pressureLabel";
    private static final String TEMP = "temperature";
    private static final String WIND = "wind";
    private static final String L_WIND = "windLabel";
    private static final String ICON = "icon";
    private static final String LAT = "latitude";
    private static final String LON = "longitude";
    private GoogleMap mMap;
    private OWMWeatherData weatherData;
    private OWMClient weatherClient;
    private TextView mTemperature;
    private TextView mHumidity;
    private TextView mHumidityLabel;
    private TextView mPressure;
    private TextView mPressureLabel;
    private TextView mWindSpeed;
    private TextView mWindSpeedLabel;
    private Bitmap bmIcon;
    private double mLatitude;
    private double mLongitude;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setWeatherDataTextViews();

        ActionBar actionBar = getSupportActionBar();
        actionBar.setCustomView(R.layout.actionbar_view);

        EditText search = (EditText) actionBar.getCustomView().findViewById(R.id.searchfield);
        search.setOnEditorActionListener(this);
        search.addTextChangedListener(this);
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM
                               | ActionBar.DISPLAY_SHOW_HOME);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(HUMIDITY_KEY, mHumidity.getText().toString());
        outState.putString(L_HUMIDITY_KEY, mHumidityLabel.getText().toString());
        outState.putString(PRESSURE, mPressure.getText().toString());
        outState.putString(L_PRESSURE, mPressureLabel.getText().toString());
        outState.putString(TEMP, mTemperature.getText().toString());
        outState.putString(WIND, mWindSpeed.getText().toString());
        outState.putString(L_WIND, mWindSpeedLabel.getText().toString());
        outState.putDouble(LAT, mLatitude);
        outState.putDouble(LON, mLongitude);
        outState.putParcelable(ICON, bmIcon);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mHumidity.setText(savedInstanceState.getString(HUMIDITY_KEY));
        mHumidityLabel.setText(savedInstanceState.getString(L_HUMIDITY_KEY));
        mPressure.setText(savedInstanceState.getString(PRESSURE));
        mPressureLabel.setText(savedInstanceState.getString(L_PRESSURE));
        mTemperature.setText(savedInstanceState.getString(TEMP));
        mWindSpeed.setText(savedInstanceState.getString(WIND));
        mWindSpeedLabel.setText(savedInstanceState.getString(L_WIND));
        mLatitude = savedInstanceState.getDouble(LAT);
        mLongitude = savedInstanceState.getDouble(LON);
        bmIcon = savedInstanceState.getParcelable(ICON);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(bmIcon != null)
            renderMarker();
    }

    private void renderMarker() {
        mMap.addMarker(new MarkerOptions()
                                .position(new LatLng(mLatitude, mLongitude)))
                                .setIcon(BitmapDescriptorFactory.fromBitmap(bmIcon));
    }

    @Override
    protected void onStart() {
        super.onStart();
        setupMapIfNeeded();
    }

    private void setupMapIfNeeded() {
        if (mMap == null) {
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMap();
        }
    }

    @Override
    public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
        new WeatherDataTask().execute(textView.getText().toString());
        return false;
    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        cleanWeatherData();
        mMap.clear();
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

    @Override
    public void afterTextChanged(Editable editable) {}

    private class WeatherDataTask extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... strings) {
            String city = strings[0];
            weatherClient = new OWMClient();
            try {
                weatherData = weatherClient.currentWeatherAtCity(city);
                if (weatherData!=null){
                    URL iconUrl = new URL(weatherData.getIcon());
                    bmIcon = BitmapFactory.decodeStream(iconUrl.openConnection().getInputStream());
                    bmIcon = Utils.getResizedBitmap(bmIcon, 120, 120);
                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            if(weatherData != null){
                fillCurrentWeatherData();
            } else {
                Toast.makeText(WeatherActivity.this, getString(R.string.city_not_found), Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void setWeatherDataTextViews() {
        mHumidity = (TextView) findViewById(R.id.textView_humidity);
        mHumidityLabel = (TextView) findViewById(R.id.label_humidity);
        mPressure = (TextView) findViewById(R.id.textView_pressure);
        mPressureLabel = (TextView) findViewById(R.id.label_pressure);
        mTemperature = (TextView) findViewById(R.id.textView_temp);
        mWindSpeed = (TextView) findViewById(R.id.textView_wind);
        mWindSpeedLabel = (TextView) findViewById(R.id.label_wind);
        //default values for lat and lon
        mLatitude = 0d;
        mLongitude = 0d;
    }

    private void cleanWeatherData(){
        mHumidity.setText(R.string.empty);
        mHumidityLabel.setText(R.string.empty);
        mPressure.setText(R.string.empty);
        mPressureLabel.setText(R.string.empty);
        mTemperature.setText(R.string.empty);
        mWindSpeed.setText(R.string.empty);
        mWindSpeedLabel.setText(R.string.empty);
        bmIcon = null;
    }

    private void fillCurrentWeatherData() {
        mHumidityLabel.setText(R.string.label_humidity);
        mPressureLabel.setText(R.string.label_pressure);
        mWindSpeedLabel.setText(R.string.label_wind);
        mLatitude = weatherData.getLat();
        mLongitude = weatherData.getLon();
        mTemperature.setText(weatherData.getTemp() + getString(R.string.celcius));
        mHumidity.setText(weatherData.getHumidity() + getString(R.string.percent));
        mPressure.setText(weatherData.getPressure() + getString(R.string.millimeter_of_mercury));
        mWindSpeed.setText(weatherData.getSpeed() + getString(R.string.km_per_hour));
        renderMarker();
        mMap.moveCamera(Utils.updateCamera(new LatLng(weatherData.getLat(), weatherData.getLon())));
    }
}

